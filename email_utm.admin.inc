<?php

/**
 * @file
 * Admin settings for Email UTM.
 */

/**
 * Admin settings page.
 */
function email_utm_settings_form() {
  $form = array();

  $parsed_base_url = parse_url($GLOBALS['base_url']);

  $form['email_utm_hosts'] = array(
    '#title' => t('Hosts'),
    '#type' => 'textarea',
    '#default_value' => variable_get('email_utm_hosts', $parsed_base_url['host']),
    '#description' => t("Enter one host per line without a scheme (e.g. http). UTM query parameters will be applied to every URL from following hosts."),
  );

  foreach (module_implements('mailkeys') as $module) {
    $info = system_get_info('module', $module);

    $form[$module] = array(
      '#type' => 'fieldset',
      '#title' => $info['name'],
      '#description' => t('Module: %name', array('%name' => $module)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $function = $module . '_mailkeys';
    foreach ($function() as $key => $description) {
      $form[$module][$key] = array(
        '#type' => 'fieldset',
        '#title' => $description,
        '#description' => t('Key: %name', array('%name' => $key)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $enabled_variable_key = implode('_', array('email_utm_enabled', $module, $key));
      $form[$module][$key][$enabled_variable_key] = array(
        '#type' => 'checkbox',
        '#title' => t('Enabled'),
        '#default_value' => variable_get($enabled_variable_key, FALSE),
      );

      $source_variable_key = implode('_', array('email_utm_source', $module, $key));
      $form[$module][$key][$source_variable_key] = array(
        '#type' => 'textfield',
        '#title' => 'utm_source',
        '#required' => TRUE,
        '#default_value' => variable_get($source_variable_key, 'drupal'),
        '#description' => t("Use utm_source to identify a search engine, newsletter name, or other source.")
      );

      $medium_variable_key = implode('_', array('email_utm_medium', $module, $key));
      $form[$module][$key][$medium_variable_key] = array(
        '#type' => 'textfield',
        '#title' => 'utm_medium',
        '#required' => TRUE,
        '#default_value' => variable_get($medium_variable_key, $module),
        '#description' => t("Use utm_medium to identify a medium such as email or cost-per- click.")
      );

      $campaign_variable_key = implode('_', array('email_utm_campaign', $module, $key));
      $form[$module][$key][$campaign_variable_key] = array(
        '#type' => 'textfield',
        '#title' => 'utm_campaign',
        '#required' => TRUE,
        '#default_value' => variable_get($campaign_variable_key, $key),
        '#description' => t("Required. Used for keyword analysis. Use utm_campaign to identify a specific product promotion or strategic campaign.")
      );

      $term_variable_key = implode('_', array('email_utm_term', $module, $key));
      $form[$module][$key][$term_variable_key] = array(
        '#type' => 'textfield',
        '#title' => 'utm_term',
        '#default_value' => variable_get($term_variable_key, ''),
        '#description' => t("Used for paid search. Use utm_term to note the keywords for this ad.")
      );

      $content_variable_key = implode('_', array('email_utm_content', $module, $key));
      $form[$module][$key][$content_variable_key] = array(
        '#type' => 'textfield',
        '#title' => 'utm_content',
        '#default_value' => variable_get($content_variable_key, ''),
        '#description' => t("Used for A/B testing and content-targeted ads. Use utm_content to differentiate ads or links that point to the same URL.")
      );
    }
  }

  return system_settings_form($form);
}
