<?php

/**
 * Implements hook_email_utm_tags().
 */
function hook_email_utm_tags_alter(&$tags, $module, $key) {
  if ($module == 'module_name') {
    $tags['utm_source'] = 'foo';
    $tags['utm_medium'] = 'bar';
    $tags['utm_campaign'] = 'baz';
    $tags['utm_term'] = 'qux';
    $tags['utm_content'] = 'quux';
  }
}
